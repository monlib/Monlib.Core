# Contributing

Hi, thanks for thinking about contributing! This project needs as much feedback as possible,
so even nitpicks are appreciated. You can always discuss or ask for help on
[Gitter][monlib-gitter].

## How you can help

- It's very important that the documentation is as clear and understandable as possible.
You can help by pointing out ambiguities and/or fixing them. It's fine to open
an issue simply because you don't know what the documentation is trying to say.


- It's always good to hear criticism of the design or suggestions for
improvement, especially before 1.0.0. You can save us all a lot of trouble if
you spot a problem early on.


- The API is lacking many features and you can help by suggesting them. In many
cases you can simply add the properties yourself, but you can always open an
issue and let somebody else figure it out. Please tell us what this feature is
useful for and where it could be implemented.

- Even small changes help, so you can always fix spelling errors, update broken
links or add other minor fixes.

## Issue vs Merge Request

If you can solve a problem youself, open a merge request. If the problem only
has one obvious solution, you don't need to create an issue for it. Otherwise
open an issue, so that other people can chime in on the problem (instead of only
your solution).

## Issue Guidelines

When opening an issue, please look for similar issues first. If you find one
and it is closed, you may ask to reopen it and continue the conversation.

## Merge Request Guidelines

### Commits

Please base your merge requests on the latest master and use `git rebase` with
`git push --force` to bring in new changes. In particular, do not merge `master`
into your MR.

Please look at https://chris.beams.io/posts/git-commit/ for commit message style.

If you are asked to make changes, you may issue fixup commits, but amending
them is preferred (`git commit --amend` or `git rebase -i`).

### Docs

The documentation is a set of reStructuredText files, which are processed by Sphinx.
Please try to write readable english and use a spellcheker (e.g.
copy-paste output into Word).

[monlib-gitter]: https://gitter.im/Monlib/Lobby
