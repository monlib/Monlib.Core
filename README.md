# Monlib.Core
[![Documentation Status](https://readthedocs.org/projects/monlibcore/badge/?version=latest)](http://monlibcore.readthedocs.io/en/latest/?badge=latest)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/Monlib/Lobby?utm_source=share-link&utm_medium=link&utm_campaign=share-link)

> Write one tool for many games

Monlib.Core is a Lua API for editing save files that allows users to write tools
which work with multiple different games, while allowing each game's quirks to be preserved.

The documentation for all released versions and the current development version are on
[readthedocs](https://monlibcore.readthedocs.io/).

## Example

```lua
function (save) -- print basic info
    local mon = save.monsterStorage.team[1]
    print("Name: %s", mon.nickname)
    print("Species ID: %d", mon.species)
    print("OriginalTrainer: %s", mon.originalTrainerName)
    print("OT ID: %d", mon.originalTrainerId)
end

function (save) -- make changes
    local box = save.monsterStorage.pcStorage[1]
    local mon = box[5]
    local newOtName = "xX" + mon.originalTrainerName + "Xx"
    local improvedMon = assert(mon.modifiedWith({originalTrainerName = newOtName}))
    local goodBox = box:withMonAt(improvedMon, 5)
    return save:withMonsterStorage(save.monsterStorage:withBoxAt(goodBox, 'team'))
end
```

## In Other Languages

Monlib.Core can be used from many different languages because most offer
either an in-language Lua implementation, Lua bindings or the ability to call C
functions.

## Benefits

### Reusability

If you want to build a save editor, you don't need to reimplement the whole save
format. Simply use a package which implements it and write your tool. Inversely,
this means that these packages will be worked on more and improvements will
reach many different tools.

### Extensibility

The API also offers extensibility, so that implementations can have
features that are not (yet) in the standard. Simply prefix your method/property
with `ext` and extend away. This allows implementations to represent all the
unique features of the save file, without forcing them into some preconceived
generalizations.

### Continuity

Monlib.Core has a well-defined versioning scheme and is thoroughly documented.
If it's in the API, you know what it can do and can rely on it being there
*at least* until the next major version. Implementations benefit in a similar
way since all new features are optional. Once you support 1.0.0, you support
any 1.* version.

### Modularity

Everybody can implement Monlib.Core for their own projects, because it is just
an API. If you want to implement some weird spin-off game, romhack, or perhaps
a game from another franchise entirely, you can just do it. And any tool that
can load implementations at runtime can then be used with that game (if it has
the needed features).

This modularity offers another benefit: tools and implementation have fewer
contact points. By programming against a common API, you are independent from
an implementation's internal details and changes.

### Accuracy

Because Monlib.Core can add new features without breaking implementations, it can
support similar concepts without abstracting their differences away. Where other
tools might hack their DVs to look like IVs, Monlib.Core distinguishes between
these two concepts since they have different semantics.

### Openness

Monlib.Core encourages contributions to add new features or refine the design.
If your tool needs a feature or your package could provide a feature, you can
always suggest it be added to the standard.

## Building

You will need Sphinx to build the documentation. Simply run `make` (or
`make.bat`) in `docs/` and pick a target. The online version uses `make html`.


## Contributing

Community feedback and contributions are incredibly important for this project.
If you want to just chat about something related to monlib, head to
[Gitter][monlib-gitter]. For other ways of contributing, look at
[CONTRIBUTING.md](./CONTRIBUTING.md).

## License

All files within this project are licensed under the GPLv3 or higher unless
otherwise noted. A copy of the license is included in the file LICENSE.
By contributing to this project you agree that your work is licensed under the
terms of the GPLv3 or any later version.

## Thanks

- [Project Pokemon](https://projectpokemon.org) for their tools and research
- [PKHeX.Core](https://github.com/kwsch/PKHeX) for being a comprehensive save
editing library.

[monlib-gitter]: https://gitter.im/Monlib/Lobby
