Reserved Names
==============

Unless otherwise noted, the following keys are reserved for future use on all
objects:

- All keys of the following types:
    - ``number``
    - ``boolean``

- All string keys which match the regex ``[a-z][a-zA-Z0-9]+`` and do not also
  match ``ext[A-Z0-9][a-zA-Z0-9]``.

Implementations may use these unreserved keys for providing their own
functionality or for implementation purposes. Extensions should be prefixed
with ``ext``.
