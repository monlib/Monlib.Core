Mutability
==========

Unless otherwise noted, all operations specified in these documents are pure
functions within the context of this API under the following definition:

    For equivalent parameters equivalent return values are produced and
    afterwards all operands are equivalent to themselves before the operation.

Should any of these conditions be broken, an error is raised.

Unless otherwise specified, users may not add, delete, or change any keys on
any objects.

Equivalence
-----------

Two objects are equivalent under the following conditions:

- If both are either ``nil``, a number, a string, or a boolean then they must
  compare equal using ``==``.

- Otherwise they must produce equivalent results for all operations defined by this API
