Versioning
==========

This specification uses SemVer.

This spec is very conscious of the fact that not all implementations can
support all operations described here. Therefore minor revisions will only
introduce optional properties or methods. Names will only be removed in major
revisions.
