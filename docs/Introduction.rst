Introduction
============

This specification describes the interfaces of objects that can be used to
access savedata. Pretty much every feature is optional, as will be noted. This
API does not care about classes, though objects usually get their functionality
that way.

Properties are not required to be settable, unless otherwise stated.
