Errors
======

Return values
-------------

Errors may be indicated by returning ``nil`` and an error message (or just
``nil`` for properties). These errors are not fatal and all objects involved
in the operation remain unchanged and can safely be reused. This failure mode
indicates a minor error that occured because any parameters or the underlying data
could not be used.

These errors are repeatable, that is if ``foo(bar)`` returns ``nil, "error"``,
then all future invocations of ``foo`` with the same parameters must return
that same error, except if an error is `raised`.

Raising
-------

Errors which are communicated via ``error`` or equivalent functions indicate that
the operation encountered a fatal error, such as exhausting memory, encountering
IO errors, detecting corrupted or invalid data, or detecting a programming error.
Monlib.Core mandates that all objects involved in the operation remain unchanged
and continue to conform to the described interfaces, but users should assume some
degree of fatality to these errors and assume that all operations on the affected
objects raise errors.

Future revisions
----------------

Users may not rely on any particular operation returning an error, since future
revisions of this API may generalize interfaces to accept parameters which
previously resulted in errors.
