Welcome to Monlib.Core's documentation!
=======================================

Monlib.Core specifies a lua interface for working with savedata from a variety
of different games, such as romhacks or the official Pokémon games.

.. toctree::
    :maxdepth: 2
    :caption: Monlib API

    Introduction
    Mutability
    Errors
    Reserved Names <ReservedNames>
    Binary Formats <BinaryFormats>
    Versioning
    Objects
