Binary Formats
==============

Binary Formats refer to the several different serialization methods for data
used by Monlib objects, such as ``.pk*`` files. These formats enable, for
example, a box from one implementation to insert monsters from another
implementation, as long as both can agree on a binary format. Formats also
provide an extension point to this API, anybody can define and implement their
own binary formats.

.. _binary-format-tags:

Binary Format Tags
------------------

Binary formats must be identified by a string of 16 bytes, a so-called
``BinaryFormatTag``. This tag must be unique to a binary format, though
the bytes themselves don't have any inherent meaning. Implementations should
use big-endian UUIDs as tags and not data that could easily conflict (e.g.
shortened package name).
