Objects
=======

.. note::

    All methods must be called with a ``self`` parameter, either using ``:``
    syntax or explicitly. The ``self`` parameter is omitted in function
    signatures for brevity.

.. py:class:: Save

    .. py:attribute:: monsterStorage

        Grants access to a save's monster storage.

        :rtype: MonsterStorage or nil

    .. py:method:: serializeToFile(file)

        Writes the save to ``file``. ``file`` is open before and after the operation.

        :param file file: A file-like object with ``flush``, ``seek``,
            ``setvbuf`` and ``write`` methods with signatures identical to
            ``io.file``. The file must be opened in write mode and must be
            capable of binary output (Add ``"b"`` to io.open on Windows). The
            file must be empty before the operation.

        :return: nil

        Example::

            local f = io.open("mysave.bin", "wb")
            if f then
                local success, errMsg = pcall(mySave.serializeToFile, mySave, f)
                f:close()
                assert(success, errMsg)
            end

    .. py:method:: serializeToString()

        Returns the save serialized as a string.

        :returns: The serialized string or nil and an error message.
        :rtype: string or (nil, string)

    .. py:method:: withMonsterStorage(storage)

        (*Optional*) Returns a copy of this :py:class:`Save` with :py:attr:`monsterStorage`
        equivalent to ``storage``.

        :param MonsterStorage storage: The new monster storage
        :rtype: Save or (nil, string)
        :returns: A save or nil and an error message


.. py:class:: MonsterStorage

    Encapsulates a set of :py:obj:`Box` es.

    .. py:method:: __index(i)

        This accessor is meant to provide access to all components that
        store monsters, such as the Team, PC and Daycare. Implementations are free
        to add more esoteric storage methods (battle videos?) or omit common ones.

        :param int i: 1-based index
        :return: A :py:obj:`Box` if i ∈ [1; #self], otherwise :py:obj:`nil`
        :rtype: Box or nil

    .. py:method:: __len()

        Length operator.

        :return: The number of :py:class:`Box` es.
        :rtype: int

    .. py:attribute:: team

        This property provides access to a save's team, without the use of the
        :py:meth:`__index` method, which may place the team at an arbitrary
        index.

        :rtype: Box or nil

    .. py:attribute:: daycare

        This property provides access to a save's daycare, in a manner similar
        to :py:attr:`team`.

        :rtype: Box or nil

    .. py:attribute:: pcStorage

        This property returns a sequence of :py:class:`Box`, which contains only
        the boxes of the PC Storage System.

        :rtype: sequence(Box) or nil

    .. py:method:: withBoxAt(box, keys [, ...])

        (*Optional*) Returns a copy of this :py:class:`MonsterStorage` with ``box``
        at ``keys``.

        ``keys`` must be one or more keys which identify where ``box`` must be placed.

        In the resulting object, the value obtained by indexing with all ``keys``
        in sequence must be equivalent to ``box``. All other sequences of keys
        which did not refer to the same :py:class:`Box` must have values equivalent
        to those of ``self``.

        :param Box box: the box to insert
        :param object keys: One or more keys identifying where to insert the box
        :rtype: MonsterStorage or (nil, string)
        :returns: A monster storage or nil and an error message

        Example::

            function gimmeTheSameBoxIfCanInsert(monsterStorage, box)
                local storage, err = monsterStorage:withBoxAt(box, 'pcStorage', 5)
                if storage then
                    return storage.pcStorage[5]
                else
                    return nil, err
                end
            end

.. py:class:: Box

    A container which holds :py:class:`Monster` s

    .. py:method:: __index(i)

        Gets the :py:class:`Monster` at the specified position.
        Accesses for i ∉ [1;  :py:attr:`capacity`] must return nil and not raise an error.

        :param int i: 1-based index
        :rtype: Monster or nil

    .. py:attribute:: name

        The box's name as stored in the save data.

        :rtype: string or nil

    .. py:attribute:: acceptedBinaryFormats

        (*Required to be not* ``nil`` *if* :py:meth:`withMonAt` *is present.*) a sequence of all
        :ref:`binary-format-tags` which can be inserted into this :py:class:`Box`.

        :rtype: table or nil

    .. py:attribute:: capacity

        The box's capacity.

        :rtype: int or nil

    .. py:method:: withMonAt(monster, index)

        (*Optional*) Returns a copy of this box with ``monster`` at ``index``.

        ``monster`` must either be ``nil`` or a :py:class:`Monster` with a
        :py:meth:`Monster.serializeTo` method and at least one element of
        :py:attr:`Monster.binaryFormats` must be in :py:attr:`acceptedBinaryFormats`.

        In the resulting box, the monster at ``index`` must be equivalent to
        ``monster`` and all other keys must have values equivalent to those of
        ``self``.

        :type monster: Monster or nil
        :param monster: the monster to insert or ``nil`` to clear the position
        :param int index: a 1-based index into the box
        :rtype: Box or (nil, string)
        :returns: A box with ``monster`` at ``index`` or nil and an error message.

.. py:class:: Monster

    A Monster.

    .. py:attribute:: species

        :rtype: SpeciesId or nil

    .. py:attribute:: form

        :rtype: FormId or nil

    .. py:attribute:: isEgg

        :rtype: bool or nil

    .. py:attribute:: isShiny

        :rtype: bool or nil

    .. py:attribute:: heldItem

        :rtype: Item or nil

    .. py:attribute:: nickname

        :rtype: string or nil

    .. py:attribute:: originalTrainerName

        :rtype: string or nil

    .. py:attribute:: originalTrainerId

        :rtype: int or nil

    .. py:attribute:: experience

        :rtype: number or nil

    .. py:attribute:: level

        Level and experience are commonly stored separately, though one is
        derived form the other. Implementations choose how to resolve
        conflicts. If the actual level as derived from the experience is
        desired, :py:attr:`computedLevel` should be used instead.

        :rtype: int or nil

    .. py:attribute:: computedLevel

        The Monster's level as derived from experience.

        :rtype: int or nil

    .. py:attribute:: editableProperties

        Returns the names of all properties which can be edited as a sequence
        of unspecified order.

        :rtype: table or nil

    .. py:method:: modifiedWith(changes[, ignoreUnknown=False])

        (*Optional*) This method takes a table of propreties and values and
        returns a :py:class:`Monster` with the specified properties or nil and
        an error message on failure.

        Neither the original :py:class:`Monster` object nor any parameters are modified.

        :param table changes: A table of key-value-pairs, where each key-value-pair
            is a possible property of :py:class:`Monster` and describes the desired
            state of the returned :py:class:`Monster`.
        :param bool ignoreUnknown: If False, the result will always be nil if
            any key in ``changes`` is not in :py:attr:`editableProperties`.
        :rtype: Monster or (nil, string)
        :return:  Either a monster where each key in
            ``changes`` has the same value as in ``changes`` or nil and an
            implementation-defined error message.

        Example::

            local changes = { computedLevel=53, originalTrainerName="xXCoolGuyXx" }
            local newMonster, failure = monster.modifiedWith(changes)
            if newMonster then
                assert(newMonster.computedLevel == 53)
                assert(newMonster.originalTrainerName == "xXCoolGuyXx")
            else
                print(failure)
            end

    .. py:attribute:: binaryFormats

        (*Required if* :py:meth:`serializeTo` *is present.*) Returns a sequence
        of :ref:`binary-format-tags`, identifying all formats this monster can
        be serialized into.

        :rtype: table or nil

    .. py:method:: serializeTo(formatTag)

        This method serializes a monster into a binary format, specified by
        ``formatTag``.

        :type formatTag: :ref:`Binary Format Tag <binary-format-tags>`
        :param formatTag: The format to serialize to. Must be in :py:attr:`binaryFormats`
        :rtype: string or (nil, string)
        :returns: Either a string containing serialized data or nil and an error message.
